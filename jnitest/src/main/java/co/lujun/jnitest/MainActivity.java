package co.lujun.jnitest;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = (TextView) findViewById(R.id.textview);
        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callShowDialogMethod();
            }
        });
        textView.setText(getStringFromNative());
    }

    public void showMessage(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Native方法调用Java方法");
        builder.setMessage("这是一个Native调用Java方法的例子！");
        builder.show();
    }

    public native String getStringFromNative();
    public native int callShowDialogMethod();

    static {
        System.loadLibrary("androidccpp");// 参数library name
    }
}
