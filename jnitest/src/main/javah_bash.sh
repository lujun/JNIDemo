#!/bin/sh
export ANDROID_JAR_PATH="F:\code\android\adt-bundle-windows-x86_64-20140702\sdk\platforms\android-23\android.jar"
export DEBUG_CLASS_PATH="..\..\build\intermediates\classes\debug"
export TargetClassName="co.lujun.jnitest.MainActivity"

javah -d jni -classpath "${ANDROID_JAR_PATH}";"${DEBUG_CLASS_PATH}" "${TargetClassName}"

#javah -d jni -classpath F:\code\android\adt-bundle-windows-x86_64-20140702\sdk\platforms\android-23\android.jar;..\..\build\intermediates\class
#es\debug co.lujun.jnitest.MainActivity
